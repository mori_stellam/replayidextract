
# DEBUG
MSGS = True

# Login info
USERNAME = ""   # Fill in this information
PASSWORD = ""   # Fill in this information

# Website address
URLLOG = "https://cpdeluxe.com/Login?ReturnUrl=%2f"
URLCONPAN = "https://cpdeluxe.com/Aspx/Interface/GameHosting/GameServers.aspx?forcemobile=false"
URLASPXNET = "https://cpdeluxe.com/Aspx/Interface/Base/Home.aspx?forcemobile=false"
URLLOGS = "https://cpdeluxe.com/Service/LogViewer/GetGameLogsRead/"


# Details for HTTP GET and POSTs
BASEHEAD = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.123 Safari/537.36',
}

# LOGS VIEW CONSTANTS
LOG_page = 1
LOG_pageSize = 100
