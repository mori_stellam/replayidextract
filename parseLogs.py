import argparse
import io
import math
import os.path
import re
from datetime import datetime, timedelta
from datetime import date as datetimeDATE


# CONFIG GLOBALS
OUTFILE = 'ReplayID_'
OUTEXT = '.log'
CURRFOLD = os.getcwd()
OUT_PAD_HEAD = 15
OUT_PAD_TEAMS = 30
DATE = ""
DEF_MAP = "Scenario_Farmhouse_Firefight_West"

# Define main classes
class comLineParser:
    def __init__(self):
        self.parser = argparse.ArgumentParser(description='Extract ReplayID using server log files.')
        self.parser.add_argument('--DATE', metavar='D', type=str, nargs=1,
            help='Specify date to be parsed. Format: %Y-%m-%d. Default value will '+
            'be the yesterday.')
        self.args = self.parser.parse_args()
        return None

class Command:
    def __init__(self, phrase: str = '', struct: str = '', note: str = '', regex: re.Pattern = re.compile('unknown'),
                tabulates: str = '', call = None):
        self.phrase = phrase
        self.struct = struct
        self.note = note
        self.regex = regex
        self.tabs = tabulates
        self.call = call
        return None

class Match:
    def __init__(s, strt="", end="", dur=-1, amap="", ID="", plrs=[], scr="", lin=-1):
        s.start = strt
        s.end = end
        s.duration = dur
        s.map = amap
        s.ID = ID
        if len(plrs)==0:
            s.players = [[],[]]
        s.playercount = 0
        s.addplayers = 0
        s.score = scr
        s.line = lin
        s.endswitch = -1


# Supporting functions
def GET_TIME(line:str) -> datetime:
    timeString = re.search("\[([0-9\.\-]+)\:[0-9]+\]\[[0-9\s]+\]", line).group(1)
    return datetime.strptime(timeString , '%Y.%m.%d-%H.%M.%S')


def out_Pugs(hostn: str, lpugs: list, date: str, hostnames: list) -> None:
    if os.path.isfile(CURRFOLD + "\\" + OUTFILE +  date + OUTEXT):
        f = open(CURRFOLD + "\\" + OUTFILE +  date + OUTEXT, "a+", encoding="utf-8")
    else:
        f = open(CURRFOLD + "\\" + OUTFILE +  date + OUTEXT, "w+", encoding="utf-8")
        f.write(" ==== Replay ID-s: "+ date + " ====\n\n\n")

    OUT_PAD_TEAMSX = 2 * OUT_PAD_TEAMS
    if hostnames[-1]!=hostn:
        f.write("".join(OUT_PAD_TEAMSX*"*") + "\n* Server: " + hostn + "\n" + "".join(OUT_PAD_TEAMSX*"*") + "\n\n" )

    for pug in lpugs:
        maxp = max(len(pug.players[0]), len(pug.players[1]))
        f.write("".join(OUT_PAD_TEAMSX*"=") + "\n")
        f.write("|" + "".join(4*'-') + " ID:".ljust(OUT_PAD_HEAD) + pug.ID + "\n")
        f.write("|" + "".join(4*'-') + " Scenario:".ljust(OUT_PAD_HEAD) + pug.map + "\n")
        f.write("|" + "".join(4*'-') + " Time:".ljust(OUT_PAD_HEAD) + pug.start.strftime("%H:%M") + "\n")
        f.write("".join(OUT_PAD_TEAMSX*'-') + "\n")
        f.write("|   TEAM0".ljust(OUT_PAD_TEAMS) + "   TEAM1".ljust(OUT_PAD_TEAMS) + "\n")
        for i in range(maxp):
            pl1 = pug.players[0][i][:OUT_PAD_TEAMS-4] if i+1<=len(pug.players[0]) else ""
            pl2 = pug.players[1][i][:OUT_PAD_TEAMS-4] if i+1<=len(pug.players[1]) else ""
            f.write(u"".join([u"| ", pl1]).ljust(OUT_PAD_TEAMS)
                   +u"".join([u"  ", pl2]).ljust(OUT_PAD_TEAMS)
                   +u"\n")
        f.write("".join(OUT_PAD_TEAMSX*'=') + "\n\n")


    f.close()
    return None


# Functions corresponding to commands
def NOFUNCTION(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    pass
    return [lmap, lpugs, lmatch]


def update_Map(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    lmap = COMMANDS['Mapchange'].regex.search(line).group(2)
    #lmatch = Match(amap=lmap)
    return [lmap, lpugs, lmatch]


def reset_Map(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    #lmatch = Match(amap=lmap)
    return [lmap, lpugs, lmatch]


def add_Player(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    if lmatch.addplayers == 1:
        if lmatch.addplayers == 10: lmatch.players = [[],[]]
        player = COMMANDS['Jointeam'].regex.search(line).group(2)
        team = int(COMMANDS['Jointeam'].regex.search(line).group(3))
        for teamID in (0,1):
            lmatch.players[teamID] = list(dict.fromkeys(lmatch.players[teamID])) # Remove duplicates
            if player in lmatch.players[teamID]:
                lmatch.players[teamID].remove(player)
        if team in (0,1):
            lmatch.players[team].append(player)
        lmatch.playercount += 1
    return [lmap, lpugs, lmatch]


def switch_Side(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    lmatch.startswitch = nline
    lmatch.addplayers = 1
    return [lmap, lpugs, lmatch]

def finish_Switch(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    lmatch.endswitch = nline
    lmatch.addplayers = 0
    return [lmap, lpugs, lmatch]

def start_Replay(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    lmatch = Match(amap=lmap)
    lmatch.ID = COMMANDS['StarUploadReplay'].regex.search(line).group(1)
    lmatch.start = GET_TIME(line)
    lmatch.addplayers = 0
    lmatch.map = lmap if lmatch.map != lmap else lmatch.map
    lmatch.line = nline
    return [lmap, lpugs, lmatch]

def end_Replay(lmap: str, lpugs: list, lmatch: Match, line: str, nline: int, date: str) -> list:
    lmatch.end = GET_TIME(line)
    diff = lmatch.end - lmatch.start
    lmatch.duration = math.floor(diff.seconds / 60)
    if (lmatch.duration>=10 and len(lmatch.players[0])>=5 and
    len(lmatch.players[1])>=5 and lmatch.start.strftime("%Y-%m-%d")==date and
    lmatch.endswitch>0):
        lpugs.append(lmatch)
    return [lmap, lpugs, lmatch]




# Dictionary of commands
COMMANDS = {
    'Mapchange':  Command('LogGameMode: ProcessServerTravel:', 'Map?scenario=XXX?Lighting=XXX',
                          'Changemap', re.compile("LogGameMode: ProcessServerTravel: (\w+)\?scenario\=(\w+)\?"),
                          '', update_Map),
    'Reload': Command('LogGameMode: ProcessServerTravel: ?restart', 'LogGameMode: ProcessServerTravel: ?restart',
                      'LogGameMode: ProcessServerTravel: ?restart',
                      re.compile("LogGameMode: ProcessServerTravel: ?restart"),
                      '\t', reset_Map),
    'Connect': Command('LogNet: Login request:', '?InitialConnecTimeout=XX?Name=XX??Password=XX userID: XX platform: XX',
                       'Connection request',
                       re.compile("LogNet\: Login request\: \?InitialConnectTimeout\=([0-9]+)\?Name=(.+)\?\?Password\=hipfire"),
                       '\t\t', NOFUNCTION),
    'Jointeam': Command('LogGameMode: Display: Player joined team',
                        "LogGameMode: Display: Player ??? 'XXX' joined team 0/1",
                        'Player joins a team',
                        re.compile("LogGameMode: Display: Player ([0-9]+) '(.+)' joined team ([0125]+)"),
                       '\t\t', add_Player),
    'SwitchSide': Command('LogGameState',
                          'LogGameState: Match State Changed from XXX to SwitchingSides',
                          'Teams switching side',
                          re.compile("LogGameState: Match State Changed from .+ to SwitchingSides"),
                         "\t", switch_Side),
    'FinishSwitch': Command('LogGameMode: Display:', 'LogGameMode: Display: Teams have been swapped.',
                            'Teams finished switching side. Do not add more players to list.',
                           re.compile("LogGameMode: Display: Teams have been swapped."),
                           "\t\t",finish_Switch),
    'StarUploadReplay': Command('LogHttpReplay: FHttpNetworkReplayStreamer',
                                'LogHttpReplay: FHttpNetworkReplayStreamer::HttpStartUploadingFinished. SessionName:',
                                'Starts uploading recording',
                                re.compile("LogHttpReplay: FHttpNetworkReplayStreamer::HttpStartUploadingFinished. SessionName: ([\w\-]+)"),
                                '\t', start_Replay),
    'FinUploadReplay': Command('LogHttpReplay: FHttpNetworkReplayStreamer',
                               'LogHttpReplay: FHttpNetworkReplayStreamer::HttpStopUploadingFinished. SessionName:',
                               'Upload replay to central server',
                               re.compile("LogHttpReplay: FHttpNetworkReplayStreamer::HttpStopUploadingFinished. SessionName: ([\w\-]+)"),
                               '\t', end_Replay)
}



def file_shortener(fname: str) -> None:
    # File handling
    extension = re.search('.+\.([a-z]+)$', fname).group(1)
    outfile = re.search('(.+)\.[a-z]+$', fname).group(1) + '_short.' + extension
    fin = open(fname, "r", encoding='utf-8')
    fout = open(outfile, "w", encoding='utf-8')


    # Parse file
    first = fin.readline()
    date_time_string = re.search('Log file open, (.* ..:..:..)', first).group(0)
    date = datetime.strptime(date_time_string, "%d/%m/%y %H:%M:%S").strftime("%Y-%m-%d")

    nLine = 1
    thismap = "Scenario_Farmhouse_Firefight_West"
    for line in fin:
        nLine += 1
        for key in COMMANDS:
            if COMMANDS[key].regex.search(line):
                fout.write(COMMANDS[key].tabs + str(nLine) + ' '
                           + COMMANDS[key].regex.search(line).group(0) + '\n')

    fin.close()
    fout.close()
    return None



def file_parser(fname: str, date: str = "", hostnames: list=[]) -> list:
    # File handling
    extension = re.search('.+\.([a-z]+)$', fname).group(1)
    outfile = re.search('(.+)\.[a-z]+$', fname).group(1) + '_short.' + extension
    fin = open(fname, "r", encoding='utf-8')

    # Parse date - Essentially never needed
    first = fin.readline()
    if date == "":
        date_time_string = re.search('Log file open, (.* ..:..:..)', first).group(1)
        date = datetime.strptime(date_time_string, "%m/%d/%y %H:%M:%S").strftime("%Y-%m-%d")

    # Set state variables
    nLine = 1
    currMap = DEF_MAP
    currMatch = Match()
    matchList = []
    hostname = ""
    # Parse file
    for line in fin:
        nLine += 1
        for key in COMMANDS:
            if hostname=="" and re.search('LogInit: Command Line: .+ \-hostname="(.+)" .+', line):
                hostname = re.search('LogInit: Command Line: .+ \-hostname="(.+)" .+', line).group(1)
            if COMMANDS[key].regex.search(line):
                [currMap, matchList, currMatch] = COMMANDS[key].call(currMap, matchList, currMatch, line, nLine, date)

    if len(matchList)>0:
        out_Pugs(hostname, matchList, date, hostnames)
        hostnames.append(hostname)
    fin.close()
    return [date, hostname, hostnames]




def main():
    # Parse arguments
    COMLINE = comLineParser()
    # Set date
    if COMLINE.args.DATE:
        date = COMLINE.args.DATE[0]
    elif DATE!="":
        date = DATE
    else:
        date = (datetimeDATE.today()-timedelta(days=1)).strftime("%Y-%m-%d")
    # Loop over log files
    for file in os.listdir(CURRFOLD):
        if file.endswith(".log") and not(OUTFILE in file):
            # Parse the file
            [date, hostname, hostnames] = file_parser(file, date, hostnames)

            # Create new folder to move the log file and prepare new filename
            if not(os.path.isdir(os.path.join(CURRFOLD, date))):
                os.mkdir(os.path.join(CURRFOLD, date))
            new_filename = "Ins_log-" \
                        + ''.join(c for c in hostname if c.isalnum()) \
                        + ".log"
            j = 0
            while os.path.isfile(os.path.join(currFold, date, new_filename)):
                # Check if file exist. If so, rename.
                j += 1
                new_filename = "Ins_log-" \
                            + ''.join(c for c in hostname if c.isalnum()) \
                            + "_" + str(j) + ".log"
            os.rename(os.path.join(CURRFOLD, file),
                     os.path.join(CURRFOLD, date, new_filename))
    return None

if __name__ == "__main__":
    main()
