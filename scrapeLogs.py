import json
import os.path
import pprint
import re
import scrape_config
import urllib.parse
from datetime import datetime, timedelta
from datetime import date as datetimeDATE
from requests import Session
from bs4 import BeautifulSoup

# CONFIG GLOBALS
CURRFOLD = os.getcwd()
DATE = ""



class ServerInfo():
    def __init__(s, ip: str='', nme: str='', servid: int=-1, status: str=''):
        s.ip = ip
        s.name = nme
        s.servid = servid
        s.status = status
        return None




def LogSession() -> list:
    ''' Log in into website to access control panel
    '''

    # Load login page and extract relevant tokens to build login info and login header
    s = Session()
    if scrape_config.MSGS: print('LogSession')
    if scrape_config.MSGS: print('- Accessing login site.')
    mpage = s.get(scrape_config.URLLOG, headers = scrape_config.BASEHEAD)
    if scrape_config.MSGS: print('- Response from login site. Scraping html data.')
    soup = BeautifulSoup(mpage.content, 'html.parser')
    reqToken = soup.find("input", attrs={"name":"__RequestVerificationToken"})['value']
    encToken = soup.find("input",attrs={"name":"__encrypted_RequireToken"})['value']
    template = soup.find("input",attrs={"name":"Template"})['value']

    # Login info
    if scrape_config.MSGS: print('- Building login information and header.')
    logInfo = {
        '__RequestVerificationToken': reqToken,
        '__encrypted_RequireToken': encToken,
        "UserName": scrape_config.USERNAME,
        "Password": scrape_config.PASSWORD,
        "Language": "en-US",
        'Template': template,
        "RememberMe": "false",
    }

    # POST log-in request
    if scrape_config.MSGS: print('- POST login data and headers')
    p = s.post(scrape_config.URLLOG, data=logInfo, headers = scrape_config.BASEHEAD,
                cookies=s.cookies)
    print(f'- Response: {p.status_code} with a message "{p.reason}."')

    if p.status_code == 200:
        return [s]
    else:
        return [None]



def accessServerInfo(s: Session) -> list:
    ''' Retrieves information of the servers to be parsed. Returns a list of server
        information.
    '''
    # ASPX.net - get ASPX session ID added to cookies
    if scrape_config.MSGS: print('Access server list')
    if scrape_config.MSGS: print('- Log in with Aspx')
    ctrl_panel = s.get(scrape_config.URLASPXNET, headers=scrape_config.BASEHEAD, cookies=s.cookies)

    # Control - Panel: ASPX
    if scrape_config.MSGS: print('- Query Aspx for server list')
    ctrl_panel = s.get(scrape_config.URLCONPAN, headers=scrape_config.BASEHEAD,
                    cookies=s.cookies)

    # Scrape server informations
    if scrape_config.MSGS: print('- Parse server list')
    control_pnl_soup = BeautifulSoup(ctrl_panel.content, 'html.parser')
    serv_list_parse = (control_pnl_soup.find_all('table', id='ContentPlaceHolderMain_GameServers1_GridGameServers_WebGrid_ctl00')[0]
                                .find_all('tr', id=re.compile("ContentPlaceHolderMain_GameServers1_GridGameServers_WebGrid_ctl00__[0-9]+"))
    )
    serv_list = []
    if scrape_config.MSGS: print(''.join([30*"-",'\n- Server List \n',30*"-"]))
    for serv in serv_list_parse:
        try:
            currServ = ServerInfo()
            currServ.ip = serv.find('a', id=re.compile("ConnectionInfo")).string
            currServ.status = serv.find('span', id=re.compile("LabelStatus")).string
            currServ.servid = int(serv.find('a', id=re.compile("ServiceId[0-9]+$")).string)
            currServ.name = serv.find_all('td')[-2].string
            serv_list.append(currServ)
            if scrape_config.MSGS: print(f'- Server: "{currServ.name}" parsed.')
        except:
            if scrape_config.MSGS:
                print('-- Error parsing following entry:')
                print(serv.prettify())
    return [s, serv_list]


def retrieveLogNames(s: Session, serv: ServerInfo, date: str) -> None:
    ''' Retrieves log files from the specified server
    '''
    if scrape_config.MSGS: print(f'\n- Acces server logs for: "{serv.name}".')
    prevDate = (datetime.strptime(date, "%Y.%m.%d") - timedelta(days=1)).strftime("%Y.%m.%d")

    # Build header
    logPostData = {
        'sort': 'LastWriteTimeUtc-desc',
        'page': str(scrape_config.LOG_page),
        'pageSize': str(scrape_config.LOG_pageSize),
        'group': '',
        'filter': '',
    }

    # Retrieve list of files
    list_logs = []
    if scrape_config.MSGS: print(f'- Link: {scrape_config.URLLOGS + str(serv.servid)}')
    log_resp = s.post(scrape_config.URLLOGS + str(serv.servid),
                    data=logPostData,
                    headers=scrape_config.BASEHEAD,
                    cookies=s.cookies)
    if scrape_config.MSGS: print(f'- Response: {log_resp.status_code} with a message "{log_resp.reason}."')

    # Parse filenames
    if scrape_config.MSGS: print(f'- Date: "{date}"')
    if scrape_config.MSGS: print(f'- Search string: "{"".join(("Insurgency-backup-", date))}"')
    for file in json.loads(log_resp.content)['Data']:
        if file['Extension']=='.log' and \
            (file['Name']=='Insurgency.log' or \
            ''.join(('Insurgency-backup-', date)) in file['Name'] or \
            ''.join(('Insurgency-backup-', prevDate)) in file['Name']):
            list_logs.append(file)
    return [s, list_logs]

def downloadLogs(s: Session, serv: ServerInfo, loglist: list) -> None:
    ''' Download logs for a given server.
    '''
    for logfile in loglist:
        QueryURL = scrape_config.URLDOWNLOGS + str(serv.servid) + '?' + \
                    urllib.parse.urlencode({'fullName': logfile['FullName']})
        if scrape_config.DIAG: print(f'Downloading logs for {serv.name}')
        if scrape_config.DIAG: print(f'- Name of file: {logfile["Name"]}')
        if scrape_config.DIAG: print(f'- GET link: {QueryURL}')
        rp = s.get(QueryURL, headers=scrape_config.BASEHEAD, cookies=s.cookies)

        if scrape_config.MSGS:  print(f'- {logfile["Name"]} Response: {rp.status_code} with a message "{rp.reason}."')
        if rp.status_code==200:
            j=0
            filename = ''.join(c for c in serv.name if c.isalnum()) +'-'+str(j)+'-' + logfile['Name']
            while os.path.isfile(os.path.join(CURRFOLD, filename)):
                j += 1
                filename = ''.join(c for c in serv.name if c.isalnum()) +'-'+str(j)+'-' + logfile['Name']
            fout = open(filename, 'w+', encoding="utf-8")
            fout.write(rp.content.decode('utf-8'))
            fout.close()
    return None



def main() -> None:
    # Reformat date if needed
    try:
        lDate = datetime.strptime(DATE, "%Y-%m-%d").strftime("%Y.%m.%d")
    except:
        lDate = DATE
    date = (datetimeDATE.today()-timedelta(days=1)).strftime("%Y.%m.%d") if lDate=="" else lDate

    [session] = LogSession()
    if session:
        [session, servers] = accessServerInfo(session)
        for server in servers:
            [session, logFiles] = retrieveLogNames(session, server, date)
            downloadLogs(session, server, logFiles)
    return None


if __name__ == "__main__":
    main()
