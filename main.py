import argparse
import os
import parseLogs
import scrapeLogs
from datetime import datetime, timedelta

OUTFILE = 'ReplayID_'
OUTEXT = '.log'
OUT_PAD_HEAD = 15
OUT_PAD_TEAMS = 30
DEF_MAP = "Scenario_Farmhouse_Firefight_West"


class comLineParser:
    def __init__(self):
        self.parser = argparse.ArgumentParser(description='Extract ReplayID using server log files.')
        self.parser.add_argument('--DATE', metavar='D', type=str, nargs=1,
            help='Specify date to be parsed. Format: %Y-%m-%d. Default value will '+
            'be the yesterday.')
        self.args = self.parser.parse_args()
        return None


def main() -> None:
    print("START")
    COMLINE = comLineParser()
    if COMLINE.args.DATE:
        date = COMLINE.args.DATE[0]
    else:
        date = date = (datetimeDATE.today()-timedelta(days=1)).strftime("%Y-%m-%d")
    dateDot = datetime.strptime(date, "%Y-%m-%d").strftime("%Y.%m.%d")

    # Download logs
    print("****************************")
    print("* Download logs")
    print("****************************")
    [req_session] = scrapeLogs.LogSession()
    if req_session:
        [req_session, servers] = scrapeLogs.accessServerInfo(req_session)
        for server in servers:
            [req_session, req_logfiles] = scrapeLogs.retrieveLogNames(req_session, server, dateDot)
            scrapeLogs.downloadLogs(req_session, server, req_logfiles)

    # Parse log files
    print("****************************")
    print("* Parse logs")
    print("****************************")
    hostnames = [""]
    currFold = os.getcwd()
    for file in os.listdir(currFold):
        if file.endswith(".log") and not(OUTFILE in file):
            # Parse the file
            print(file)
            [date, hostname, hostnames] = parseLogs.file_parser(file, date, hostnames)
            # Create new folder to move the log file and prepare new filename
            if not(os.path.isdir(os.path.join(currFold, date))):
                os.mkdir(os.path.join(currFold, date))
            new_filename = "Ins_log-" \
                        + ''.join(c for c in hostname if c.isalnum()) \
                        + ".log"
            j = 0
            while os.path.isfile(os.path.join(currFold, date, new_filename)):
                # Check if file exist. If so, rename.
                j += 1
                new_filename = "Ins_log-" \
                            + ''.join(c for c in hostname if c.isalnum()) \
                            + "_" + str(j) + ".log"
            os.rename(os.path.join(currFold, file),
                     os.path.join(currFold, date, new_filename))
    return None

if __name__=="__main__":
    main()
